package me.thewoosh.plugintestutilities.mocks;

import junit.framework.TestCase;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerTeleportEvent;

public class FakeEntityTest extends TestCase {

    public void testGetLocation() {
        FakeEntity entity = new FakeEntity();
        assertNotNull(entity.getLocation());
    }

    public void testGetLocationDestination() {
        Location location = new Location(FakeWorld.getGlobalInstance(), 0, 1, 2);
        FakeEntity entity = new FakeEntity();
        assertEquals(entity.getLocation(location), location);
        assertEquals(entity.getLocation(), location);
    }

    public void testTeleport() {
        FakeEntity entity = new FakeEntity();
        Location location = entity.getLocation().clone().add(1, 1, 1);
        assertTrue(entity.teleport(location));
        assertEquals(location, entity.getLocation());
    }

    public void testTeleportWithCause() {
        FakeEntity entity = new FakeEntity();
        Location location = entity.getLocation().clone().add(1, 1, 1);
        assertTrue(entity.teleport(location, PlayerTeleportEvent.TeleportCause.SPECTATE));
        assertEquals(location, entity.getLocation());
    }

    public void testTeleportWithEntity() {
        FakeEntity entity = new FakeEntity();
        FakeEntity entityTo = new FakeEntity();
        Location location = entityTo.getLocation().clone().add(1, 1, 1);
        entityTo.teleport(location);

        assertTrue(entity.teleport(entityTo));
        assertEquals(entity.getLocation(), entityTo.getLocation());
    }

    public void testTeleportWithEntityAndCause() {
        FakeEntity entity = new FakeEntity();
        FakeEntity entityTo = new FakeEntity();
        Location location = entityTo.getLocation().clone().add(1, 1, 1);
        entityTo.teleport(location, PlayerTeleportEvent.TeleportCause.END_GATEWAY);

        assertTrue(entity.teleport(entityTo));
        assertEquals(entity.getLocation(), entityTo.getLocation());
    }
}