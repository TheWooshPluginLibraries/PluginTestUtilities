package me.thewoosh.plugintestutilities.item;

import me.thewoosh.plugintestutilities.item.meta.FakeItemMeta;
import me.thewoosh.plugintestutilities.item.meta.special.FakeLeatherArmorMeta;
import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.junit.Test;

import static org.junit.Assert.*;

public class FakeItemFactoryTest {

    private final FakeItemFactory factory = new FakeItemFactory();

    /**
     * FakeItemFactory guarantees that the item meta isn't null
     */
    @Test
    public void testGetItemMeta() {
        assertNotNull(factory.getItemMeta(Material.STONE));
    }

    @Test
    public void testGetLeather() {
        ItemMeta itemMeta = factory.getItemMeta(Material.LEATHER_BOOTS);
        assertNotNull(itemMeta);
        assertTrue(itemMeta instanceof LeatherArmorMeta);
        assertEquals(factory.getDefaultLeatherColor(), ((LeatherArmorMeta) itemMeta).getColor());
    }

    @Test
    public void testGetPotion() {
        for (Material material : new Material[] { Material.POTION, Material.SPLASH_POTION, Material.LINGERING_POTION, Material.TIPPED_ARROW }) {
            ItemMeta itemMeta = factory.getItemMeta(material);
            assertNotNull(itemMeta);
            assertTrue(itemMeta instanceof PotionMeta);
        }
    }

}