package me.thewoosh.plugintestutilities.enchantments;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;

public class EnchantmentsRegistry {

    private static boolean isInitialized = false;

    /**
     * We can't do reflection of net.minecraft.server.Enchantments.<clinit>()
     * unfortunately, due to the fact we're importing the Spigot/Bukkit API,
     * which excludes the NMS package.
     *
     * Because of this, were gonna have to register the enchantments manually.
     */
    @SuppressWarnings("deprecation")
    public static boolean register() {
        if (isInitialized) {
            return false;
        }

        isInitialized = true;
        String[] keys = {
            "protection",
            "fire_protection",
            "feather_falling",
            "blast_protection",
            "projectile_protection",
            "respiration",
            "aqua_affinity",
            "thorns",
            "depth_strider",
            "frost_walker",
            "binding_curse",
            "soul_speed",
            "sharpness",
            "smite",
            "bane_of_arthropods",
            "knockback",
            "fire_aspect",
            "looting",
            "sweeping",
            "efficiency",
            "silk_touch",
            "unbreaking",
            "fortune",
            "power",
            "punch",
            "flame",
            "infinity",
            "luck_of_the_sea",
            "lure",
            "loyalty",
            "impaling",
            "riptide",
            "channeling",
            "multishot",
            "quick_charge",
            "piercing",
            "mending",
            "vanishing_curse"
        };

        for (String key : keys) {
            Enchantment.registerEnchantment(new FakeEnchantment(key, EnchantmentTarget.ALL));
        }

        return true;
    }

}
