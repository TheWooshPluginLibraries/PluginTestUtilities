package me.thewoosh.plugintestutilities.item.meta.special;

import me.thewoosh.plugintestutilities.item.meta.FakeItemMeta;
import org.bukkit.Color;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.*;

public class FakePotionMeta extends FakeItemMeta implements PotionMeta {

    private Color color = null;
    private PotionData potionData = new PotionData(PotionType.AWKWARD);
    private final ArrayList<PotionEffect> customEffects = new ArrayList<>();

    @Override
    public void setBasePotionData(PotionData data) {
        this.potionData = data;
    }

    @Override
    public PotionData getBasePotionData() {
        return potionData;
    }

    @Override
    public boolean hasCustomEffects() {
        return !customEffects.isEmpty();
    }

    @Override
    public List<PotionEffect> getCustomEffects() {
        return customEffects;
    }

    @Override
    public boolean addCustomEffect(PotionEffect effect, boolean overwrite) {
        for (PotionEffect customEffect : customEffects) {
            if (customEffect.getType() == effect.getType()) {
                if (overwrite) {
                    customEffects.remove(effect);
                    break;
                }

                return false;
            }
        }

        customEffects.add(effect);
        return true;
    }

    @Override
    public boolean removeCustomEffect(PotionEffectType type) {
        return customEffects.removeIf((effect) -> effect.getType() == type);
    }

    @Override
    public boolean hasCustomEffect(PotionEffectType type) {
        for (PotionEffect effect : customEffects) {
            if (effect.getType() == type) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean setMainEffect(PotionEffectType type) {
        // TODO
        return false;
    }

    @Override
    public boolean clearCustomEffects() {
        boolean wasEmpty = customEffects.isEmpty();
        customEffects.clear();
        return !wasEmpty;
    }

    @Override
    public boolean hasColor() {
        return color != null;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public PotionMeta clone() {
        return super.clone(new FakePotionMeta());
    }

}
