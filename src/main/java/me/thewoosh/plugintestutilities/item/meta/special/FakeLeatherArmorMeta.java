package me.thewoosh.plugintestutilities.item.meta.special;

import me.thewoosh.plugintestutilities.item.meta.FakeItemMeta;
import org.bukkit.Color;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class FakeLeatherArmorMeta extends FakeItemMeta implements LeatherArmorMeta {

    private Color color;

    public FakeLeatherArmorMeta(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public LeatherArmorMeta clone() {
        return super.clone(new FakeLeatherArmorMeta(color));
    }

}
