package me.thewoosh.plugintestutilities.item.meta;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.*;

public class FakeItemMeta implements ItemMeta {

    private Multimap<Attribute, AttributeModifier> attributeModifiers = HashMultimap.create();
    private final FakeCustomItemTagContainer customTagContainer = (FakeCustomItemTagContainer) FakeItemTagAdapterContext.getInstance().newTagContainer();
    private Integer customModelData = null;
    private String displayName = null;
    private final HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    private final Set<ItemFlag> flags = new HashSet<>();
    private String localizedName = null;
    private List<String> lore = null;
    private final FakePersistentDataContainer persistentDataContainer = (FakePersistentDataContainer) FakePersistentDataAdapterContext.getInstance().newPersistentDataContainer();
    private boolean unbreakable = false;

    @Override
    public boolean hasDisplayName() {
        return displayName != null;
    }

    @Override
    public String getDisplayName() {
        if (displayName == null) {
            throw new NullPointerException("hasDisplayName() is false! Check before calling this function.");
        }

        return displayName;
    }

    @Override
    public void setDisplayName(String name) {
        this.displayName = name;
    }

    @Override
    public boolean hasLocalizedName() {
        return localizedName != null;
    }

    @Override
    public String getLocalizedName() {
        if (localizedName == null) {
            throw new NullPointerException("hasLocalizedName() is false! Check before calling this function.");
        }

        return localizedName;
    }

    @Override
    public void setLocalizedName(String name) {
        this.localizedName = name;
    }

    @Override
    public boolean hasLore() {
        return lore != null;
    }

    @Override
    public List<String> getLore() {
        return lore;
    }

    @Override
    public void setLore(List<String> lore) {
        this.lore = lore;
    }

    @Override
    public boolean hasCustomModelData() {
        return customModelData != null;
    }

    @Override
    public int getCustomModelData() {
        return customModelData;
    }

    @Override
    public void setCustomModelData(Integer data) {
        this.customModelData = data;
    }

    @Override
    public boolean hasEnchants() {
        return !enchantments.isEmpty();
    }

    @Override
    public boolean hasEnchant(Enchantment ench) {
        return enchantments.containsKey(ench);
    }

    @Override
    public int getEnchantLevel(Enchantment ench) {
        return enchantments.get(ench);
    }

    @Override
    public Map<Enchantment, Integer> getEnchants() {
        return enchantments;
    }

    @Override
    public boolean addEnchant(Enchantment ench, int level, boolean ignoreLevelRestriction) {
        if (enchantments.containsKey(ench)) {
            return false;
        }

        enchantments.put(ench, level);
        return true;
    }

    @Override
    public boolean removeEnchant(Enchantment ench) {
        if (!enchantments.containsKey(ench)) {
            return false;
        }

        enchantments.remove(ench);
        return true;
    }

    @Override
    public boolean hasConflictingEnchant(Enchantment ench) {
        return false;
    }

    @Override
    public void addItemFlags(ItemFlag... itemFlags) {
        flags.addAll(Arrays.asList(itemFlags));
    }

    @Override
    public void removeItemFlags(ItemFlag... itemFlags) {
        flags.removeAll(Arrays.asList(itemFlags));
    }

    @Override
    public Set<ItemFlag> getItemFlags() {
        return flags;
    }

    @Override
    public boolean hasItemFlag(ItemFlag flag) {
        return flags.contains(flag);
    }

    @Override
    public boolean isUnbreakable() {
        return unbreakable;
    }

    @Override
    public void setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
    }

    @Override
    public boolean hasAttributeModifiers() {
        return !attributeModifiers.isEmpty();
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers() {
        return attributeModifiers;
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot slot) {
        Multimap<Attribute, AttributeModifier> attributeModifiers = HashMultimap.create();

        if (this.attributeModifiers == null) {
            return attributeModifiers;
        }

        for (Map.Entry<Attribute, AttributeModifier> entry : this.attributeModifiers.entries()) {
            if (entry.getValue().getSlot() == slot) {
                attributeModifiers.put(entry.getKey(), entry.getValue());
            }
        }

        return attributeModifiers;
    }

    @Override
    public Collection<AttributeModifier> getAttributeModifiers(Attribute attribute) {
        return attributeModifiers == null ? null : attributeModifiers.get(attribute);
    }

    @Override
    public boolean addAttributeModifier(Attribute attribute, AttributeModifier modifier) {
        return attributeModifiers.put(attribute, modifier);
    }

    @Override
    public void setAttributeModifiers(Multimap<Attribute, AttributeModifier> attributeModifiers) {
        this.attributeModifiers = attributeModifiers;
    }

    @Override
    public boolean removeAttributeModifier(Attribute attribute) {
        if (attributeModifiers == null) {
            return false;
        }

        return attributeModifiers.entries().removeIf(entry -> entry.getKey() == attribute);
    }

    @Override
    public boolean removeAttributeModifier(EquipmentSlot slot) {
        return attributeModifiers.entries().removeIf(entry -> entry.getValue().getSlot() == slot);
    }

    @Override
    public boolean removeAttributeModifier(Attribute attribute, AttributeModifier modifier) {
        return attributeModifiers.remove(attribute, modifier);
    }

    @Override
    public CustomItemTagContainer getCustomTagContainer() {
        return customTagContainer;
    }

    @Override
    public void setVersion(int version) {
        // nop
    }

    public <T extends FakeItemMeta> T clone(T dest) {
        FakeItemMeta instance = (FakeItemMeta) dest;

        instance.attributeModifiers = this.hasAttributeModifiers() ? HashMultimap.create(this.attributeModifiers) : null;
        instance.customTagContainer.copyFrom(this.customTagContainer);
        instance.customModelData = customModelData;
        instance.displayName = displayName;
        instance.enchantments.putAll(enchantments);
        instance.flags.addAll(this.flags);
        instance.localizedName = localizedName;
        instance.lore = this.hasLore() ? new ArrayList<>(this.lore) : null;
        instance.persistentDataContainer.addAll(this.persistentDataContainer.getMap());
        instance.unbreakable = unbreakable;

        return dest;
    }

    @Override
    public ItemMeta clone() {
        try {
            return clone((FakeItemMeta) super.clone());
        } catch (CloneNotSupportedException e) {
            return clone(new FakeItemMeta());
        }
    }

    @Override
    public Map<String, Object> serialize() {
        return new HashMap<>();
    }

    @Override
    public PersistentDataContainer getPersistentDataContainer() {
        return persistentDataContainer;
    }

}
