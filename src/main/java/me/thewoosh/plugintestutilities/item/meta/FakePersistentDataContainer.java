package me.thewoosh.plugintestutilities.item.meta;

import org.bukkit.NamespacedKey;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.HashMap;
import java.util.Set;

/**
 * Implementation of PersistentDataContainer.
 *
 * WARNING: This is an unsafe class. Exceptions on get() may be throw because of unsafe casts.
 */
public class FakePersistentDataContainer implements PersistentDataContainer {

    private final HashMap<NamespacedKey, Object> map = new HashMap<>();

    @Override
    public <T, Z> void set(NamespacedKey key, PersistentDataType<T, Z> type, Z value) {
        map.put(key, value);
    }

    @Override
    public <T, Z> boolean has(NamespacedKey key, PersistentDataType<T, Z> type) {
        return map.containsKey(key);
    }

    @Override
    public <T, Z> Z get(NamespacedKey key, PersistentDataType<T, Z> type) {
        return (Z) map.get(key);
    }

    @Override
    public <T, Z> Z getOrDefault(NamespacedKey key, PersistentDataType<T, Z> type, Z defaultValue) {
        return (Z) map.get(key);
    }

    @Override
    public Set<NamespacedKey> getKeys() {
        return map.keySet();
    }

    @Override
    public void remove(NamespacedKey key) {
        map.remove(key);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    public HashMap<NamespacedKey, Object> getMap() {
        return map;
    }

    public void addAll(final HashMap<NamespacedKey, Object> src) {
        map.putAll(src);
    }

    @Override
    public PersistentDataAdapterContext getAdapterContext() {
        return FakePersistentDataAdapterContext.getInstance();
    }

}
